$(document).ready(function(){
  var button = '<input type="button" value="'+Drupal.t('Fetch address')+'" class="reverse-geocode" /><div class="reverse-geocode-throbber ahah-progress ahah-progress-throbber throbber-hidden"><div class="throbber">&nbsp;</div><div class="message">'+Drupal.t('Please wait...')+'</div></div>';

  $(button).insertAfter('#node-form .gmap-gmap');

	$('.location_auto_country').change(
    function() {
      var $auto = $(this).parent().siblings('.autocomplete');
      $auto.siblings().find('input').unbind();
      $auto.val($auto.val().substr(0, $auto.val().lastIndexOf('/') + 1) + $(this).val()).removeClass('autocomplete-processed');
      Drupal.behaviors.autocomplete(document);
	  }
	);
  
	$('.reverse-geocode').click(function(){
    var initial_object = $(this);
    $(initial_object).siblings(".reverse-geocode-throbber").css('display','block');
    
    var lat = $(this).siblings("div[id*=locpick_latitude]").children('input').val().toString();
    var lon = $(this).siblings("div[id*=locpick_longitude]").children('input').val().toString();

    lat = lat.replace(".",",");
    lon = lon.replace(".",",");

    var dmn = base_url();

    if(lat=="" || lon==""){
      alert("Invalid coordinates. Please pick a location on the map or input coordinates manually.");
      $(initial_object).siblings(".reverse-geocode-throbber").css('display','none');
    }
    else{
      $.get(dmn+'/geocode/reverse/'+lat+'|'+lon, function(data) {
        var parsed = JSON.parse(data);

        // Uncomment the code block below to see all values that are returned from reverse_geocode.module.
        
        /*			
        var msg = "";
        $.each(parsed, function(key, value) {
          msg+="The key is '" + key + "' and the value is '" + value + "' \n";
        });

        alert(msg);
        */

        var full_address = (parsed["full_address"]==null) ? "" : parsed["full_address"];
        var street_number= (parsed["street_number"]==null) ? "" : parsed["street_number"];
        var route        = (parsed["route"]==null) ? "" : parsed["route"];
        var sublocal     = (parsed["sublocality"]==null) ? "" : parsed["sublocality"];
        var local        = (parsed["locality"]==null)? "" : parsed["locality"];
        var admin_area_1 = (parsed["administrative_area_level_1"]==null) ? "" : parsed["administrative_area_level_1"];
        var admin_area_2 = (parsed["administrative_area_level_2"]==null) ? "" : parsed["administrative_area_level_2"];
        var admin_area_3 = (parsed["administrative_area_level_3"]==null) ? "" : ", " + parsed["administrative_area_level_3"];
        var country      = (parsed["country"]==null) ? "" : parsed["country"];
        var postal_code	 = (parsed["postal_code"]==null) ? "" : parsed["postal_code"];

        var fullcity;
        if(sublocal.length != 0 && local.length != 0)
          fullcity = sublocal + ', ' + local;
        else{
          if (sublocal)
            fullcity = admin_area_2 + ', ' + sublocal;
          else 
            fullcity = admin_area_2 + ', ' + local;
        }

				$('.location_auto_country').trigger('change');
        
				$(initial_object).siblings("div[id*=name-wrapper]").children('input').attr("value", full_address);
        $(initial_object).siblings("div[id*=street-wrapper]").children('input').attr("value", route);
        $(initial_object).siblings("div[id*=additional-wrapper]").children('input').attr("value", street_number);
        $(initial_object).siblings("div[id*=city-wrapper]").children('input').attr("value", fullcity);
        $(initial_object).siblings("div[id*=province-wrapper]").children('input').attr("value", admin_area_1);
        $(initial_object).siblings("div[id*=postal-code-wrapper]").children('input').attr("value", postal_code);
        $(initial_object).siblings("div[id*=country-wrapper]").children('select').attr("value", country.toLowerCase());
   			$(initial_object).siblings(".reverse-geocode-throbber").css('display','none');

				
      });
    }
  });
})


function base_url() {
  var strUrl = location.href; 
  var strBaseURL = strUrl.substring(0, strUrl.indexOf('/', 14));

	if (strBaseURL.indexOf('http://localhost') != -1) {
		var strUrl = location.href; 
		var strPathName = location.pathname; 
		var intIndex1 = strUrl.indexOf(strPathName);
		var intIndex2 = strUrl.indexOf("/",intIndex1 + 1);
		var strBaseLocalUrl = strUrl.substr(0, intIndex2);
		return strBaseLocalUrl;
	}
	else {
		return strBaseURL;
	}
}
